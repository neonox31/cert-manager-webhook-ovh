module gitlab.com/neonox31/cert-manager-webhook-ovh

go 1.14

require (
	github.com/bobesa/go-domain-util v0.0.0-20190911083921-4033b5f7dd89
	github.com/jetstack/cert-manager v0.14.3
	github.com/ovh/go-ovh v1.1.0
	k8s.io/apiextensions-apiserver v0.17.4
	k8s.io/apimachinery v0.17.4
	k8s.io/client-go v0.17.4
	k8s.io/klog v1.0.0
)
