# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.0.3](https://gitlab.com/neonox31/cert-manager-webhook-ovh/compare/1.0.2...1.0.3) (2021-01-24)


### Bug Fixes

* **ovh:** debug challenge ([f01a93a](https://gitlab.com/neonox31/cert-manager-webhook-ovh/commit/f01a93ab2e1efea5f699c7e0f74dd44c1a1d4e0e))

### [1.0.2](https://gitlab.com/neonox31/cert-manager-webhook-ovh/compare/1.0.1...1.0.2) (2021-01-24)


### Bug Fixes

* **ovh:** get correct OVH zone and subdomain ([58f7ecb](https://gitlab.com/neonox31/cert-manager-webhook-ovh/commit/58f7ecb672a85fcdf9ed81485ba8290fff49d8f4))

### [1.0.1](https://gitlab.com/neonox31/cert-manager-webhook-ovh/compare/1.0.0...1.0.1) (2021-01-24)


### Bug Fixes

* **ovh:** add debug information for challenge present ([e34e251](https://gitlab.com/neonox31/cert-manager-webhook-ovh/commit/e34e251ef971d012c2ec2b81d9015faf8f78c397))

## 1.0.0 (2020-11-15)


### Features

* add arm64 platform ([34ede10](https://gitlab.com/neonox31/cert-manager-webhook-ovh/commit/34ede1082f90787cc112d7d11401d2f8da618cb9))
* **dependencies:** upgrade to 1.1.0 version of go-ovh ([f8c2138](https://gitlab.com/neonox31/cert-manager-webhook-ovh/commit/f8c213888ade5d0d144daa50e40f3bf24d793f02))
* **dependencies:** upgrade to 1.17.4 version of kubernetes ([7970df3](https://gitlab.com/neonox31/cert-manager-webhook-ovh/commit/7970df32f60bd948b12a4112fbfd3204893498a8))


### Bug Fixes

* **deploy:** furnish a manifest and remove kustomize section from README ([34298c5](https://gitlab.com/neonox31/cert-manager-webhook-ovh/commit/34298c5365da18d6e93f6c2136b17f30b6ec1eaa))
