# cert-manager-webhook-ovh

Cert-manager ACME DNS webhook provider for OVH.

This webhook was created following https://github.com/jetstack/cert-manager-webhook-example instructions.

## Install

### Install cert manager
Please find document here: https://cert-manager.io/docs/installation/kubernetes/

### Install cert-manager-webhook-ovh

1. Deploy cert-manager-webhook-ovh using manifest in [releases](https://gitlab.com/neonox31/cert-manager-webhook-ovh/-/releases) page

```bash
curl https://f001.backblazeb2.com/file/neonox31-public/gitlab/cert-manager-webhook-ovh/<tag>/cert-manager-webhook-ovh.yaml | kubectl apply -f -
```
⚠ This manifest is provided with `acme.example.com` default domain, it should be overlayed to inject your proper domain.

2. [Create a new OVH token](https://eu.api.ovh.com/createToken/) with the following rights:
* `GET /domain/zone/*`
* `PUT /domain/zone/*`
* `POST /domain/zone/*`
* `DELETE /domain/zone/*`

3. Create secret contains OVH credentials

```bash
  kubectl create secret generic ovh-credentials \
    --from-literal=appKey='<OVH_APPLICATION_KEY>' \
    --from-literal=appSecret='<OVH_APPLICATION_SECRET>' \
    --from-literal=consumerKey='<OVH_CONSUMER_KEY>'
```

4. Example Issuer
```yaml
  apiVersion: cert-manager.io/v1alpha2
  kind: Issuer
  metadata:
    name: letsencrypt-staging
  spec:
    acme:
      # Change to your letsencrypt email
      email: certmaster@example.com
      server: https://acme-staging-v02.api.letsencrypt.org/directory
      privateKeySecretRef:
        name: letsencrypt-staging
      solvers:
      - dns01:
          webhook:
            # Change groupName according to the one defined in step 1
            groupName: acme.example.com
            solverName: ovh
            config:
              endpoint: "ovh-eu"
              appKeySecretRef:
                key: appKey
                name: ovh-credentials
              appSecretSecretRef:
                key: appSecret
                name: ovh-credentials
              consumerKeySecretRef:
                key: consumerKey
                name: ovh-credentials
 ```

5. Issue a certificate
```yaml
apiVersion: cert-manager.io/v1alpha2
kind: Certificate
metadata:
  name: example-com
spec:
  dnsNames:
    - '*.example.com'
  issuerRef:
    name: letsencrypt-staging
    kind: Issuer
  secretName: example-com-tls
```
