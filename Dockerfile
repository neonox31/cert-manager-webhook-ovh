FROM alpine

ARG TARGETARCH

RUN apk add --no-cache ca-certificates

COPY dist/cert-manager-webhook-ovh-linux-$TARGETARCH /usr/local/bin/webhook

ENTRYPOINT ["webhook"]
