package main

import (
	"fmt"
	"github.com/bobesa/go-domain-util/domainutil"
	"github.com/jetstack/cert-manager/pkg/acme/webhook/apis/acme/v1alpha1"
	"github.com/jetstack/cert-manager/pkg/issuer/acme/dns/util"
	"github.com/ovh/go-ovh/ovh"
	"k8s.io/klog"
	"strconv"
)

type OvhCredentials struct {
	AppKey      string `json:"appKey"`
	AppSecret   string `json:"appSecret"`
	ConsumerKey string `json:"consumerKey"`
}

type OvhDomainRecord struct {
	Id        int    `json:"id,omitempty"`
	Target    string `json:"target"`
	SubDomain string `json:"subDomain"`
	FieldType string `json:"fieldType,omitempty"`
	TTL       int    `json:"ttl,omitempty"`
}

type OvhClient struct {
	*ovh.Client
}

func (c *OvhDNSProviderSolver) getOVHCredentials(cfg *OvhDNSProviderConfig, namespace string) (*OvhCredentials, error) {
	var credentials OvhCredentials

	appKey, err := c.readSecretValue(namespace, cfg.AppKeySecretRef.Name, cfg.AppKeySecretRef.Key)
	if err != nil {
		return nil, err
	}
	credentials.AppKey = *appKey

	appSecret, err := c.readSecretValue(namespace, cfg.AppSecretSecretRef.Name, cfg.AppSecretSecretRef.Key)
	if err != nil {
		return nil, err
	}
	credentials.AppSecret = *appSecret

	consumerKey, err := c.readSecretValue(namespace, cfg.ConsumerKeySecretRef.Name, cfg.ConsumerKeySecretRef.Key)
	if err != nil {
		return nil, err
	}
	credentials.ConsumerKey = *consumerKey

	return &credentials, nil
}

func (c *OvhDNSProviderSolver) NewOvhClientFromChallenge(ch *v1alpha1.ChallengeRequest) (*OvhClient, error) {
	cfg, err := loadConfig(ch.Config)
	if err != nil {
		return nil, err
	}

	klog.Infof("Decoded config: %v", cfg)

	credentials, err := c.getOVHCredentials(&cfg, ch.ResourceNamespace)
	if err != nil {
		return nil, fmt.Errorf("OVH credentials fetch failed: %v", err)
	}

	client, err := ovh.NewClient(cfg.Endpoint, credentials.AppKey, credentials.AppSecret, credentials.ConsumerKey)
	if err != nil {
		return nil, err
	}
	return &OvhClient{client}, nil
}

func (c *OvhClient) getTXTRecord(zone string, subDomain string, value string) (int, error) {
	var recordIds []int

	if err := c.Get("/domain/zone/"+zone+"/record?fieldType=TXT&subDomain="+subDomain, &recordIds); err != nil {
		return -1, err
	}

	for _, recordId := range recordIds {
		record := OvhDomainRecord{}
		if err := c.Get("/domain/zone/"+zone+"/record/"+strconv.Itoa(recordId), &record); err != nil {
			return -1, err
		}
		if record.Target == value {
			return record.Id, nil
		}
	}
	return -1, nil
}

func (c *OvhClient) addTXTRecord(zone string, subDomain string, target string) error {
	params := &OvhDomainRecord{SubDomain: subDomain, FieldType: "TXT", Target: target, TTL: 60}
	klog.Infof("Add TXT record with following params `%v`", params)
	if err := c.Post("/domain/zone/"+zone+"/record", params, nil); err != nil {
		return err
	}
	return c.refreshZone(zone)
}

func (c *OvhClient) updateTXTRecord(zone string, subDomain string, recordId int, target string) error {
	params := &OvhDomainRecord{SubDomain: subDomain, Target: target, TTL: 60}
	klog.Infof("Update existing TXT with following params `%v`", params)
	if err := c.Put("/domain/zone/"+zone+"/record/"+strconv.Itoa(recordId), params, nil); err != nil {
		return err
	}
	return c.refreshZone(zone)
}

func (c *OvhClient) deleteTXTRecord(zone string, recordId int) error {
	klog.Infof("Delete existing TXT record on zone \"%s\"", zone)
	if err := c.Delete("/domain/zone/"+zone+"/record/"+strconv.Itoa(recordId), nil); err != nil {
		return err
	}
	return c.refreshZone(zone)
}

func (c *OvhClient) refreshZone(zone string) error {
	klog.Infof("Refresh \"%s\" zone", zone)
	if err := c.Post("/domain/zone/"+zone+"/refresh", nil, nil); err != nil {
		return err
	}
	return nil
}

func getOVHSubDomain(fqdn string) string {
	return domainutil.Subdomain(util.UnFqdn(fqdn))
}

func getOVHZone(fqdn string) string {
	return domainutil.Domain(util.UnFqdn(fqdn))
}