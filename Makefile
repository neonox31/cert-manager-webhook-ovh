SRC_DIR=src
BUILD_DIR=dist
BIN_NAME=cert-manager-webhook-ovh
GOOS:=$(shell go env GOOS)
GOARCH:=$(shell go env GOARCH)

help: # Print help on Makefile
	@grep '^[^.#]\+:\s\+.*#' Makefile | \
	sed "s/\(.\+\):\s*\(.*\) #\s*\(.*\)/`printf "\033[93m"`\1`printf "\033[0m"`	\3 [\2]/" | \
	expand -t20

clean: # Clean generated files and test cache
	@rm -rf $(BUILD_DIR)
	@go clean -testcache

fmt: # Format source code
	@go fmt ./$(SRC_DIR)/...

lint: # Lint source code
	@golangci-lint --color always --timeout 10m run ./$(SRC_DIR)/...

test: clean # Run unit tests
	@go test -cover ./$(SRC_DIR)/...

.PHONY: build
build: clean # Build binary
	@mkdir -p $(BUILD_DIR)
	@go build -ldflags "-s -f" -o $(BUILD_DIR)/$(BIN_NAME)-$(GOOS)-$(GOARCH) ./$(SRC_DIR)

release: # Release a new version
	@docker run --rm -it \
				-v "$(HOME)/.gnupg/pubring.kbx:/root/.gnupg/pubring.kbx" \
				-v "$(HOME)/.gnupg/trustdb.gpg:/root/.gnupg/trustdb.gpg" \
			    -v $$(gpgconf --list-dirs agent-extra-socket):/root/.gnupg/S.gpg-agent:z \
                -v ~/.gitconfig:/etc/gitconfig \
                -v $$(pwd):/workspace \
                neonox31/releaser:1.0.0 \
				$(RELEASER_ARGS)
